function problem3(inventory){
    let alphabetOrder = "";
    let carNames = [];

    
    for(let x of inventory){
        carNames.push(x.car_model)
    }

    carNames.sort() 
    
    for(let x in carNames){
        alphabetOrder += carNames[x]+"\n";
    }

    return alphabetOrder;
}

exports.problem3 = problem3;
