function problem2(inventory){
    let lastObject = inventory[inventory.length-1];
    return `Last car is a ${lastObject.car_make} ${lastObject.car_model}`
    
 }

 exports.problem2 = problem2;
