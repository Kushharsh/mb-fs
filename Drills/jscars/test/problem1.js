function problem(inventory,id)
{
    let dataItem
    for(dataItem of inventory)
    {
        if(dataItem.id == id)
        {
            return "Car 33 is a "+dataItem.id+" "+dataItem.car_year+" "+dataItem.car_make+" "+dataItem.car_model
        }
    }
}
exports.problem1 = problem;